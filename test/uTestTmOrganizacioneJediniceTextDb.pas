unit uTestTmOrganizacioneJediniceTextDb;

interface

uses
  TestFramework, System.Classes, Db, Model.Interfaces, DbClient,
  Model.OrganizacioneJediniceTextDb;

type
  TestTmOrganizacioneJediniceTextDb = class(TTestCase)
  strict private
    FmOrganizacioneJediniceTextDb: TmOrganizacioneJediniceTextDb;
  public
    procedure SetUp; override;
    procedure TearDown; override;
  published
    procedure TestGetNazivOdeljenja;
  end;

implementation

uses
  Forms;

procedure TestTmOrganizacioneJediniceTextDb.SetUp;
begin
  FmOrganizacioneJediniceTextDb := TmOrganizacioneJediniceTextDb.Create;
end;

procedure TestTmOrganizacioneJediniceTextDb.TearDown;
begin
  FmOrganizacioneJediniceTextDb.Free;
  FmOrganizacioneJediniceTextDb := nil;
end;

procedure TestTmOrganizacioneJediniceTextDb.TestGetNazivOdeljenja;
var
  ReturnValue: string;
begin
  ReturnValue := FmOrganizacioneJediniceTextDb.GetDbFileName('c:\nekidir\out\bin\imeaplikacije.exe');

  Check(ReturnValue = 'c:\nekidir\db\imeaplikacije.txt', 'putanja do fajla nije ispravna!');
end;

initialization
  RegisterTest(TestTmOrganizacioneJediniceTextDb.Suite);

end.

