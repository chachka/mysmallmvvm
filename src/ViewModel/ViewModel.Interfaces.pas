unit ViewModel.Interfaces;

interface

uses
  Db, DbClient, Graphics;

type
  IvmOrganizacioneJedinice = interface
    ['{4684D854-9863-4B3B-B6B5-7B534D839D97}']
    procedure OpenClose;
    procedure Save;
    function IndicatorColor: TColor;
    function RowColor(aField: TField): TColor;
    function GetClientDataSet: TClientDataSet;
    function GetButtonCaption: string;
  end;

implementation

end.
