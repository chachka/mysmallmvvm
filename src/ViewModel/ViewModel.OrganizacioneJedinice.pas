unit ViewModel.OrganizacioneJedinice;

interface

uses
  Db, Datasnap.DBClient, Graphics, Model.Interfaces, ViewModel.Interfaces;

type
  TvmOrganizacioneJedinice = class(TInterfacedObject, IvmOrganizacioneJedinice)
  private
    fModel: ImOrganizacioneJedinice;
    function GetColor(const aContent: string): TColor;
  public
    constructor Create();
    procedure OpenClose;
    procedure Save;
    function IndicatorColor: TColor;
    function RowColor(aField: TField): TColor;
    function GetClientDataSet: TClientDataSet;
    function GetButtonCaption: string;
  end;

function vmOrganizacioneJedinice: IvmOrganizacioneJedinice;

implementation

uses Model.OrganizacioneJedinice;
//uses Model.OrganizacioneJediniceTextDb;

function vmOrganizacioneJedinice: IvmOrganizacioneJedinice;
begin
  Result := TvmOrganizacioneJedinice.Create;
end;

constructor TvmOrganizacioneJedinice.Create;
begin
  inherited Create;
  fModel := mOrganizacioneJedinice;
  OpenClose;
end;

function TvmOrganizacioneJedinice.GetButtonCaption: string;
begin
  if fModel.Active then
    Result := 'Close'
  else
    Result := 'Open';
end;

function TvmOrganizacioneJedinice.GetClientDataSet: TClientDataSet;
begin
  Result := fModel.GetClientDataSet;
end;

function TvmOrganizacioneJedinice.GetColor(const aContent: string): TColor;
begin
  if Pos('INTERNO', aContent) > 0 then
    Result := clRed
  else if Pos('UPRAVA', aContent) > 0 then
    Result := clGreen
  else
    Result := clBlue;
end;

function TvmOrganizacioneJedinice.IndicatorColor: TColor;
begin
  Result := GetColor(fModel.GetNazivOdeljenja);
end;

procedure TvmOrganizacioneJedinice.OpenClose;
begin
  if fModel.Active then
    fModel.Close
  else
    fModel.Open;
end;

function TvmOrganizacioneJedinice.RowColor(aField: TField): TColor;
begin
  if aField.FieldName = 'ime_organizacione_jedinice' then
    Result := GetColor(aField.AsString)
  else
    Result := clBlack;
end;

procedure TvmOrganizacioneJedinice.Save;
begin
  fModel.Save;
end;

end.


