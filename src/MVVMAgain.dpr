program MVVMAgain;

uses
  Vcl.Forms,
  View.Main in 'View\View.Main.pas' {Form1},
  Model.BazaPodataka in 'Model\Model.BazaPodataka.pas' {BazaPodataka: TDataModule},
  Model.OrganizacioneJediniceTextDb in 'Model\Model.OrganizacioneJediniceTextDb.pas',
  Model.Interfaces in 'Model\Model.Interfaces.pas',
  ViewModel.Interfaces in 'ViewModel\ViewModel.Interfaces.pas',
  Model.OrganizacioneJedinice in 'Model\Model.OrganizacioneJedinice.pas' {OrganizacioneJediniceM: TDataModule},
  ViewModel.OrganizacioneJedinice in 'ViewModel\ViewModel.OrganizacioneJedinice.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TBazaPodataka, BazaPodataka);
  Application.CreateForm(TForm1, Form1);
  Application.Run;
end.
