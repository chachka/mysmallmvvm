unit View.Main;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB,
  Vcl.StdCtrls, Vcl.ExtCtrls, Vcl.Grids, Vcl.DBGrids, Vcl.DBCtrls,
  ViewModel.Interfaces;

type
  TForm1 = class(TForm)
    Panel1: TPanel;
    Button1: TButton;
    DataSource: TDataSource;
    DBGrid1: TDBGrid;
    Button2: TButton;
    DBNavigator1: TDBNavigator;
    Indicator: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure DataSourceStateChange(Sender: TObject);
    procedure DBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
  private
    fViewModel: IvmOrganizacioneJedinice;
  end;

var
  Form1: TForm1;

implementation

uses
  ViewModel.OrganizacioneJedinice;

{$R *.dfm}

procedure TForm1.FormCreate(Sender: TObject);
begin
  fViewModel := vmOrganizacioneJedinice;
  DataSource.DataSet := fViewModel.GetClientDataSet;
end;

procedure TForm1.Button1Click(Sender: TObject);
begin
  fViewModel.OpenClose;
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
  fViewModel.Save;
end;

procedure TForm1.DataSourceDataChange(Sender: TObject; Field: TField);
begin
  Indicator.Font.Color := fViewModel.IndicatorColor;
end;

procedure TForm1.DataSourceStateChange(Sender: TObject);
begin
  Button1.Caption := fViewModel.GetButtonCaption;
end;

procedure TForm1.DBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  DBGrid1.Canvas.Font.Color := fViewModel.RowColor(Column.Field);
  DBGrid1.DefaultDrawColumnCell(Rect, DataCol, Column, State);
end;

end.
