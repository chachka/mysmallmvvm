unit Model.OrganizacioneJedinice;

interface

uses
  System.SysUtils, System.Classes, Datasnap.DBClient, Datasnap.Provider,
  Data.DB, MemDS, DBAccess, PgAccess, Model.Interfaces;

type
  TOrganizacioneJediniceM = class(TDataModule, ImOrganizacioneJedinice)
    PgQuery: TPgQuery;
    DataSetProvider: TDataSetProvider;
    ClientDataSet: TClientDataSet;
    procedure DataModuleCreate(Sender: TObject);
  public
    procedure Open;
    function GetClientDataSet: TClientDataSet;
    function GetNazivOdeljenja: string;
    procedure Close;
    function Active: Boolean;
    procedure Save;
  end;

function mOrganizacioneJedinice: ImOrganizacioneJedinice;

implementation

{%CLASSGROUP 'System.Classes.TPersistent'}

{$R *.dfm}

uses Model.BazaPodataka;

function mOrganizacioneJedinice: ImOrganizacioneJedinice;
begin
  Result := TOrganizacioneJediniceM.Create(nil)
end;

{ TOrganizacioneJediniceM }

function TOrganizacioneJediniceM.Active: Boolean;
begin
  Result := ClientDataSet.Active;
end;

procedure TOrganizacioneJediniceM.Close;
begin
  ClientDataSet.Close;
end;

procedure TOrganizacioneJediniceM.DataModuleCreate(Sender: TObject);
begin
  PgQuery.Connection := BazaPodataka.PgConnection;
end;

function TOrganizacioneJediniceM.GetClientDataSet: TClientDataSet;
begin
  Result := ClientDataSet;
end;

function TOrganizacioneJediniceM.GetNazivOdeljenja: string;
begin
  Result := ClientDataSet.Fields[1].AsString;
end;

procedure TOrganizacioneJediniceM.Open;
begin
  ClientDataSet.Open;
end;

procedure TOrganizacioneJediniceM.Save;
begin
  if ClientDataSet.State in dsEditModes then
    ClientDataSet.Post;
  if ClientDataSet.ChangeCount > 0 then
    ClientDataSet.ApplyUpdates(0);
end;

end.
