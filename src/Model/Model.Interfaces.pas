unit Model.Interfaces;

interface

uses DbClient, System.Classes;

type
  ImOrganizacioneJedinice = interface
    ['{22DFCCFD-D21C-4602-9958-5B01D4F15847}']
    procedure Open;
    procedure Close;
    function Active: Boolean;
    procedure Save;
    function GetClientDataSet: TClientDataSet;
    function GetNazivOdeljenja: string;
  end;

implementation

end.
