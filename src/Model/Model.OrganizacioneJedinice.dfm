object OrganizacioneJediniceM: TOrganizacioneJediniceM
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Height = 278
  Width = 147
  object PgQuery: TPgQuery
    Connection = BazaPodataka.PgConnection
    SQL.Strings = (
      'SELECT '
      '  oznaka_organizacione_jedinice,'
      '  ime_organizacione_jedinice,'
      '  oznaka_nadredjene_organizacione_jedinice'
      'FROM '
      '  public.organizacione_jedinice'
      'order by oznaka_organizacione_jedinice')
    Left = 48
    Top = 40
  end
  object DataSetProvider: TDataSetProvider
    DataSet = PgQuery
    Left = 48
    Top = 96
  end
  object ClientDataSet: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'DataSetProvider'
    Left = 48
    Top = 152
  end
end
