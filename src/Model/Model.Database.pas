unit Model.Database;

interface

uses
  PgAccess;

type
  TDatabase = class
  private
    fConnection: TPgConnection;
  public
    property Connection: TPgConnection read fConnection;
    constructor Create;
    destructor Destroy; override;
  end;

implementation

{ TDatabase }

constructor TDatabase.Create;
begin
  inherited;

  fConnection := TPgConnection.Create(nil);

  fConnection.Database := 'test';
  fConnection.LoginPrompt := False;
  fConnection.Password := 'aria1481';
  fConnection.Port := 5494;
  fConnection.Server := 'localhost';
  fConnection.Username := 'srdjan.mijatov';

  fConnection.Open;
end;

destructor TDatabase.Destroy;
begin
  fConnection.Free;
  inherited;
end;

end.
