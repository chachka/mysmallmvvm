unit Model.OrganizacioneJediniceTextDb;

interface

uses Db, DbClient, System.Classes, Model.Interfaces;

type
  TmOrganizacioneJediniceTextDb = class(TInterfacedObject, ImOrganizacioneJedinice)
  private
    fClientDataSet: TClientDataSet;

  public
    constructor Create();
    destructor Destroy; override;

    function GetDbFileName(const aExeName: string): string;

  public
    procedure Open;
    procedure Close;
    function Active: Boolean;
    procedure Save;
    function GetClientDataSet: TClientDataSet;
    function GetNazivOdeljenja: string;
  end;

function mOrganizacioneJedinice: ImOrganizacioneJedinice;

implementation

uses System.SysUtils, Forms, Dialogs, IOUtils;

function mOrganizacioneJedinice: ImOrganizacioneJedinice;
begin
  Result := TmOrganizacioneJediniceTextDb.Create;
end;

{ TmOrganizacioneJediniceTextDb }

function TmOrganizacioneJediniceTextDb.Active: Boolean;
begin
  Result := fClientDataSet.Active;
end;

procedure TmOrganizacioneJediniceTextDb.Close;
begin
  fClientDataSet.First;
  while not fClientDataSet.Eof do
    fClientDataSet.Delete;
  fClientDataSet.Close;
end;

constructor TmOrganizacioneJediniceTextDb.Create();
begin
  inherited Create;
  fClientDataSet := TClientDataSet.Create(nil);
  fClientDataSet.FieldDefs.Add('oznaka_organizacione_jedinice', ftInteger);
  fClientDataSet.FieldDefs.Add('ime_organizacione_jedinice', ftString, 50);
  fClientDataSet.FieldDefs.Add('oznaka_nadredjene_organizacione_jedinice', ftInteger);
  fClientDataSet.CreateDataSet;
  fClientDataSet.Close;
end;

destructor TmOrganizacioneJediniceTextDb.Destroy;
begin
  fClientDataSet.Free;
  inherited;
end;

function TmOrganizacioneJediniceTextDb.GetDbFileName(const aExeName: string): string;
begin
//  Result := ExtractFileDir(ExcludeTrailingPathDelimiter(ExtractFileDir(ExcludeTrailingPathDelimiter(ExtractFileDir(aExeName))))) + PathDelim + 'db' + PathDelim + ChangeFileExt(ExtractFileName(aExeName), '.txt');
  Result := TDirectory.GetParent(TDirectory.GetParent(TDirectory.GetParent(aExeName))) + PathDelim + 'db' + PathDelim + ChangeFileExt(ExtractFileName(aExeName), '.txt');
end;

function TmOrganizacioneJediniceTextDb.GetClientDataSet: TClientDataSet;
begin
  Result := fClientDataSet;
end;

function TmOrganizacioneJediniceTextDb.GetNazivOdeljenja: string;
begin
  Result := fClientDataSet.Fields[1].AsString;
end;

procedure TmOrganizacioneJediniceTextDb.Open;
var
  lFile: TStringList;
  lLine: string;
  i: Integer;
  lDbFile: String;
begin
  i := 0;
  fClientDataSet.Open;
  lFile := TStringList.Create;
  try
  lDbFile := GetDbFileName(Application.ExeName);
    lFile.LoadFromFile(lDbFile);
    for lLine in lFile do begin
      Inc(i);
      fClientDataSet.Append;
      fClientDataSet.Fields[0].AsInteger := i;
      fClientDataSet.Fields[1].AsString := lLine;
      fClientDataSet.Fields[2].AsInteger := 0;
    end;
    fClientDataSet.First;
  finally
    lFile.Free;
  end;
end;

procedure TmOrganizacioneJediniceTextDb.Save;
begin
  // not implemented yet
end;

end.
