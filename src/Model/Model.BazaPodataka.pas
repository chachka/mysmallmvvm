unit Model.BazaPodataka;

interface

uses
  System.SysUtils, System.Classes, Data.DB, DBAccess, PgAccess;

type
  TBazaPodataka = class(TDataModule)
    PgConnection: TPgConnection;
    procedure DataModuleCreate(Sender: TObject);
  end;

var
  BazaPodataka: TBazaPodataka;

implementation

{%CLASSGROUP 'System.Classes.TPersistent'}

{$R *.dfm}

procedure TBazaPodataka.DataModuleCreate(Sender: TObject);
begin
  PgConnection.Open;
end;

end.
