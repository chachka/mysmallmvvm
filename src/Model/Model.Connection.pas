unit Model.Connection;

interface

uses
  PgAccess;

type
  TDatabase = class
  private
    fConnection: TPgConnection;
  public
    property Connection: TPgConnection read fConnection;
  end;

implementation

end.
