create table public.organizacione_jedinice (
    oznaka_organizacione_jedinice integer not null,
    ime_organizacione_jedinice varchar(100) not null,
    oznaka_nadredjene_organizacione_jedinice integer,
    constraint pk_oje primary key(oznaka_organizacione_jedinice),
    constraint ch_oje check (oznaka_organizacione_jedinice <> oznaka_nadredjene_organizacione_jedinice),
    constraint fk_oje_oje foreign key (oznaka_nadredjene_organizacione_jedinice) 
        references public.organizacione_jedinice(oznaka_organizacione_jedinice)
        on delete restrict on update cascade
); 